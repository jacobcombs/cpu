module reg3b (
	input wire clk, reset, enable,
	input wire [2:0] in,
	output reg [2:0] out
);

always @(posedge clk, negedge reset) begin
	if (reset == 0) out = 3'b000;
	else if (enable) out = in;
end

endmodule
