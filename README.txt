The goal of this project is to design a simple CPU using the Verilog HDL. 


The CPU includes the components listed below. For details, see the 
specifications in cs252_p1.html.

* Control Unit (CU)
* Program Counter (PC)
* Arithmetic Logic Unit (ALU)
* Memory (RAM)
* Accumulator Register (ACC)
* Memory Address Register (MAR)
* Instruction Register (IR)


I have designed and tested Verilog modules for most of the components listed:

Component    Module   Test Bencmark
---------    ------   -------------
PC           pc.v     pc_tb.v
ALU          alu.v    alu_tb.v
ACC, MAR *   reg3b.v  reg3b_tb.v
IR           reg2b.v  reg2b_tb.v
CU **        cu.v     cu_tb.v
RAM ***      -        -

* The ACC and MAR components have identical functionality (and the IR is
similar), so we only need one Verilog module for these two.

** The CU is given in the specifications for this project as a state table. I 
have implemented the CU in Verilog, but the code to test it is not complete. 
In fact, it is not obvious how I would even go about testing this component, 
because the Verilog module is basically just the state table from the project 
specification.

*** I have not implemented the Memory (RAM) component.


---------------------------
To run the test benchmarks:

The following commands to test the Verilog modules will run on cwolf. To test 
a different module, replace "pc" in the example below with a different module 
name.

$ module=pc
$ iverilog -o ${module}.vvp ${module}.v ${module}_tb.v
$ vvp ${module}.vvp


About the output:

The test benckmarks print neat tables such that each column represents a port, 
and each row represents a different point in simulation time.

For the ALU, every possible input combination is tested. The test prints a 
table divided into four sections, one for each ALU operation.

For the 2-bit and 3-bit register components (reg2b and reg3b), the test prints 
a table in two parts: the first is a test of the 'reset' and 'enable' ports, 
and the second is a test of the 'in' port with 'enable' on.

For the PC, there is a test of the 'enable' and 'reset' ports, followed by a 
test of the component with 'jump' set to zero (calculating PC+1), which is  
followed by a test with 'jump' set to one (meaning PC should be set to the 
jump address).
