module cu_tb();

reg clk, reset;
reg [1:0] opcode;
wire [2:0] state;
wire mar, acc, pc, ir, jump;
wire [1:0] aluop;

integer N;

cu M1(clk, reset, opcode, state, mar, acc, pc, ir, jump, aluop);

initial begin
	$dumpfile("cu.vcd");
	$dumpvars(0, cu_tb);
	$display(
		"                time clk reset opcode state mar acc pc ir jump aluop"
		);
	$monitor(
		$time, "   %b     %b     %b   %b   %b   %b  %b  %b    %b    %b", 
		clk, reset, opcode, state, mar, acc, pc, ir, jump, aluop
		);
end

always begin
	#5 clk = ~clk;
end

initial begin
	// How should this be tested?
	// It's encoded as a state table, so it seems silly to try to print out
	// that state table.
	#10 $finish;
end

endmodule
