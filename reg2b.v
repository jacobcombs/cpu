module reg2b (
	input wire clk, reset, enable,
	input wire [1:0] in,
	output reg [1:0] out
);

always @(posedge clk, negedge reset) begin
	if (reset == 0) out = 2'b00;
	else if (enable) out = in;
end

endmodule
