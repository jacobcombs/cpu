module cu (
	input wire clk, reset,
	input wire [1:0] opcode,
	output reg [2:0] state,
	output reg mar, acc, pc, ir, jump,
	output reg [1:0] aluop
);

reg [9:0] out;

/*
wire [4:0] in;
assign in = {opcode, state};

assign mar  <= (in[2:1] == 3'b001) ? 1 : 0;
assign ir   <= (in[2:1] == 3'b010) ? 1 : 0;
assign pc   <= (in[1:0] == 2'b11)  ? 1 : 0;
assign acc  <= (in[2:1] == 3'b101) ? 1 : 0;
assign jump <= (in[2:1] == 2'b11)  ? 1 : 0;
assign aluop <= (
	(in[2:1] == 2'b11) || (in[4:1] == 4'b0110)) ? 00
	: (in[4:1] == 4'b0010) ? 10
	: xx
);
*/
always @(posedge clk, negedge reset) begin
	if (reset == 0) state = 3'b000;
	else begin
		out = {state, mar, acc, pc, ir, jump, aluop};
		case ({opcode, state})
			5'b00000 : out = 10'b00100000xx;
			5'b00001 : out = 10'b01010000xx;
			5'b00010 : out = 10'b01100010xx;
			5'b00011 : out = 10'b10000100xx;
			5'b00100 : out = 10'b1010000010;
			5'b00101 : out = 10'b0010100010;
			5'b00110 : out = 10'b1110000100;
			5'b00111 : out = 10'b0010010100;

			5'b01000 : out = 10'b00100000xx;
			5'b01001 : out = 10'b01010000xx;
			5'b01010 : out = 10'b01100010xx;
			5'b01011 : out = 10'b10000100xx;
			5'b01100 : out = 10'b1010000000;
			5'b01101 : out = 10'b0010100000;
			5'b01110 : out = 10'b1110000100;
			5'b01111 : out = 10'b0010010100;

			5'b10000 : out = 10'b00100000xx;
			5'b10001 : out = 10'b01010000xx;
			5'b10010 : out = 10'b01100010xx;
			5'b10011 : out = 10'b01100100xx;
			5'b10100 : out = 10'b10100000xx;
			5'b10101 : out = 10'b00101000xx;
			5'b10110 : out = 10'b1110000100;
			5'b10111 : out = 10'b0010010100;
			
			5'b11000 : out = 10'b00100000xx;
			5'b11001 : out = 10'b01010000xx;
			5'b11010 : out = 10'b01100010xx;
			5'b11011 : out = 10'b11000100xx;
			5'b11100 : out = 10'b10100000xx;
			5'b11101 : out = 10'b00101000xx;
			5'b11110 : out = 10'b1110000100;
			5'b11111 : out = 10'b0010010100;
		endcase
	end
end

endmodule
