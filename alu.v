module alu (
	input wire [1:0] ctl,
	input wire [2:0] a, b,
	output reg [2:0] res,
	output reg of
);

always @(*) begin
	case (ctl)
		2'd0 : begin
			res <= a ~& b;
			of <= 0;
		end
		2'd1 : begin
			res <= a ~| b;
			of <= 0;
		end
		2'd2 : begin
			res = a + b;
			of = (res[2] ^ a[2]) & (res[2] ^ b[2]);
		end
		2'd3 : begin
			res = a - b;
			of = (a[2] ^ b[2]) & (a[2] ^ res[2]);
		end
	endcase
end

endmodule
