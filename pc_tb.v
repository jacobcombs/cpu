module pc_tb();

reg clk, reset, enable, jump;
reg [2:0] jaddr;
wire [2:0] addr;

integer N;

pc M1(clk, reset, enable, jump, jaddr, addr);

initial begin
	$dumpfile("pc.vcd");
	$dumpvars(0, pc_tb);
	$display("                time clk reset enable jump jaddr addr");
	$monitor($time, "   %b     %b      %b    %b   %b  %b", 
		clk, reset, enable, jump, jaddr, addr);
end

always begin
	#5 clk = ~clk;
end

initial begin
	{clk, reset, enable, jump, jaddr} = 7'b0101111;
	/* 
	 * Test reset and enable.
	 * - out should be zero upon reset negedge and on each subsequent clk 
	 *   posedge until reset = 1 again. 
	 * - out should not change on clock while enable = 0, except by reset.
	 */
	$display("Test: reset, enable");
	#10 {reset, enable} = 2'b00;
	#10 {reset, enable} = 2'b11;
	#10 {reset, enable} = 2'b01;
	#10 {reset, enable} = 2'b11;
	/*
	 * Test jump = 0 (with reset = enable = 1).
	 * - addr should take on 1 more than the previous value of addr on clk 
	 *   posedge.
	 */
	#10 {jump, jaddr} = 4'b0000;
	$display("Test: jump = 0");
	for (N = 1; N < 8; N = N + 1)
		#10;
	/*
	 * Test jaddr, where jump = 1 (with reset = enable = 1).
	 * - addr should take on the value of jaddr.
	 */
	#10 {jump, jaddr} = 4'b1000;
	$display("Test: jump = 1");
	for (N = 1; N < 8; N = N + 1)
		#10 jaddr = jaddr + 1; 
	#5 $finish;
end

endmodule
