module pc (
	input wire clk, reset, enable, jump,
	input wire [2:0] jaddr,
	output reg [2:0] addr
);

always @(posedge clk, negedge reset) begin
	if (reset == 0) addr = 3'b000;
	else if (enable == 1) begin
		if (jump == 1) addr = jaddr;
		else addr = addr + 1;
	end
end

endmodule
