module reg2b_tb();

reg clk, reset, enable;
reg [1:0] in;
wire [1:0] out;

integer N;

reg2b M1(clk, reset, enable, in, out);

initial begin
	$dumpfile("reg2b.vcd");
	$dumpvars(0, reg2b_tb);
	$display("                time clk reset enable in out");
	$monitor($time, "   %b     %b      %b %b  %b", 
		clk, reset, enable, in, out);
end

always begin
	#5 clk = ~clk;
end

initial begin
	{clk, reset, enable, in} = 5'b01011;
	/* 
	 * Test reset and enable.
	 * - out should be zero upon reset negedge and on each subsequent clk 
	 *   posedge until reset = 1 again. 
	 * - out should not change on clock while enable = 0, except by reset.
	 */
	$display("Test: reset, enable");
	#10 {reset, enable} = 2'b00;
	#10 {reset, enable} = 2'b11;
	#10 {reset, enable} = 2'b01;
	#10 {reset, enable} = 2'b11;
	/*
	 * Test in (with reset = enable = 1).
	 * - out should take on the value of in on clk posedge.
	 */
	#10 in = 0;
	$display("Test: in");
	for (N = 1; N < 4; N = N + 1)
		#10 in = in + 1;
	#5 $finish;
end

endmodule
