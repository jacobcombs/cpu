module alu_tb();

reg [1:0] ctl;
reg [2:0] a, b;
wire [2:0] res;
wire of;

integer N;

alu M1(ctl, a, b, res, of);

initial begin
	$dumpfile("alu.vcd");
	$dumpvars(0, alu_tb);
	$display("                time ctl   a   b res of");
	$monitor($time, "  %b %b %b %b  %b", ctl, a, b, res, of);
end

initial begin
	{a, b} = 6'b000000;
	for (N = 1; N < 4 * 8 * 8; N = N + 1)
		#10 {a, b} = {a, b} + 1;
	#10 $finish;
end

initial begin
	ctl = 2'b00;
	$display("00: NAND");
	#640 ctl = 2'b01;
	$display("01: NOR");
	#640 ctl = 2'b10;
	$display("10: +");
	#640 ctl = 2'b11;
	$display("11: -");
end

endmodule
